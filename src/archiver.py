import datetime
import io
import os
import zipfile
import urllib3
import notification_sender as ns
import utils.config as cf


class Archiver:
    """
    Class which handle compressinng and checking operation.

    Attributes
    ----------
    critical_nb: int
        Number of critical occured while processing. It is like a boolean,
        on the ground program quit when encounter at least on critical.
    config: string
        configuration filename
    port: int
        port used to connect to web server
    tgz_name: string
        name of tar.gz file to post on server
    zipfile: Zipfile object
        zip file object to manage zip
    zip_name: string
        zip name to get on web server
    file_name: string
        Dump filename
    time_to_save: int
        time to get files stored on sftp server (in days)
    notification_sender: log object
        object to manage logfile, e-mail and mattermost notification

    Methods
    -------
    get_config():
        Get configurations from config file and make sure that values are availables.
    get_zip():
        Make GET HTTP request to get zip file on web server.
    create_zip(data):
        Create zip file from data got from web server.
    compare_date():
        Get last modified date of file (name) and zip (zfile) and compare with
        today's date. If it is the same it means file has been change earlier
        today.
    extract_zip():
        Extract zip.
    has_file():
        Check if zip contains dump file.
    compress_to_tgz():
        Compress file to .tgz.
    clean(sftp):
        Clean all the folder,
        and closing all connections still opened

    """

    def __init__(self, port_, config_):
        """
        __init__ Constructor

        Parameters
        ----------
        port_ : int
            http port
        config_ : string
            filepath to config file
        """
        self.config = config_
        self.port = port_

        self.tgz_name = datetime.datetime.now().date().strftime("%Y%d%m") + ".tgz"
        self.zipfile = None
        self.zip_name = None
        self.file_name = None

        self.time_to_save = None
        self.notification_sender = None
        self.get_config()

    def get_config(self):
        """
        get_config

        load config file
        ----------
        Return :
            None
        """
        self.notification_sender = ns.NotificationSender(
            "./config.ini", "always")
        self.zip_name = cf.get_info(self.config, "http", "zip")
        self.file_name = cf.get_info(self.config, "http", "file")

        try:
            time_to_save_ = int(cf.get_info(self.config, "smb", "timetosave"))
        except ValueError:
            time_to_save_ = 10
            self.notification_sender.warning("Ini Error",
                                             "error while reading Time to save value format is not supported. Default value is 10 days.",)
        self.time_to_save = time_to_save_

    def get_zip(self):
        """
        get_zip

        Retrieve zipfile from http server

        Raises
        ------
        urllib3.exceptions.ResponseError
            Log all error with http connection
        """
        http = urllib3.PoolManager()
        req = None
        try:
            url = "http://localhost:" + \
                str(self.port) + "/Web-serv/" + self.zip_name
            req = http.request("GET", url)
            if req.status == 404:
                raise urllib3.exceptions.ResponseError()
            self.create_zip(req.data)

        except urllib3.exceptions.ConnectTimeoutError:
            self.notification_sender.error(
                "Connection Timeout error. Please retry.")
        except urllib3.exceptions.ConnectionError:
            self.notification_sender.error("Request ZIP",
                                           "Connection refused. Please check the destination IP adress.")
        except urllib3.exceptions.ResponseError:
            self.notification_sender.error(
                "Request ZIP", self.zip_name+" not found")
        except urllib3.exceptions.RequestError:
            self.notification_sender.error("Request ZIP",
                                           "Connection refused. Please check the destination IP adress.")
        except urllib3.exceptions.TimeoutError:
            self.notification_sender.error("Request ZIP",
                                           "Cannot connect, Please check the destination IP adress or thz zip name.")
        except urllib3.exceptions.SSLError:
            self.notification_sender.error(
                "Request ZIP", "SSL verification failed")

    def create_zip(self, data):
        """
        createZip
        Create zip file from bytes stream.
        Parameters
        ----------
        data : Bytestream
            request response
        Returns
        -------
        None
        """

        try:
            self.zipfile = zipfile.ZipFile(io.BytesIO(data), "r")
        except (zipfile.BadZipFile, zipfile.LargeZipFile):
            self.zipfile = None

    def compare_date(self):
        """
        Get last modified date of file
        and zip  and compare with today's date.
        Returns
        -------
        boolean:
            Return if file date is not the same than today - file has been changed today.
        """

        if self.zipfile is None:
            return False

        infos = self.zipfile.infolist()
        i = 0
        file_date = None
        for i, _ in enumerate(infos):
            if infos[i].filename == self.file_name:
                file_date = datetime.datetime(*infos[i].date_time[0:3])

        if file_date is not None:
            same_date = file_date.date() == datetime.datetime.now().date()
            if same_date:
                self.notification_sender.warning(
                    "Compare dates", "Modification dates are the same."
                )
            else:
                self.notification_sender.info(
                    "Compare dates", "Modification dates are differents."
                )
            return same_date

        return False

    def has_file(self):
        """
        has_file

        check if the zip file has the requested file

        Returns
        -------
        boolean
        """
        if self.zipfile is not None:
            for file in self.zipfile.infolist():
                if file.filename == self.file_name:
                    self.notification_sender.info(
                        "Zip has file", "contain "+self.file_name)
                    return True
                self.notification_sender.error(
                    "Zip has file", "has not file "+self.file_name)
                return False
        else:
            self.notification_sender.error(
                "Zip has file", " zip is empty, abord operation")
            return False

    def extract_zip(self):
        """
        Extract zip.
        Returns
        -------
        None.
        """

        if self.zipfile is None:
            self.notification_sender.error(
                "Zip extracted", "ZIP file does not exist.")
            return
        try:
            self.zipfile.extractall(os.getcwd())
            self.notification_sender.info("Zip extracted")

        except zipfile.BadZipFile:
            self.notification_sender.error(
                "Zip extracted", "Bad ZIP file. ZIP file not extracted."
            )

        except zipfile.LargeZipFile:
            self.notification_sender.error(
                "Zip extracted",
                "ZIP file would require ZIP64 functionality but\
                that has not been enabled. ZIP file not extracted.")
        finally:
            self.zipfile.close()

    def compress_to_tgz(self):
        """
        compress_to_tgz

        compress file to tgz format

        Returns
        -------
        None.
        """
        err = os.system(
            'tar -czf "'
            + os.getcwd()
            + "/"
            + self.tgz_name
            + '" "'
            + self.file_name
            + '"')
        if err != 0:
            self.notification_sender.error(
                "Compress file", "Fail to compress " + self.file_name + "file.")
        else :
            self.notification_sender.info(
                "Compress file", "compress " + self.file_name + "file succesfully.")
        if os.path.exists(os.getcwd() + "/" + self.file_name):
            os.remove(os.getcwd() + "/" + self.file_name)

    def clean(self, sftp):
        """
        Clean all the folder

        Parmeters
        ---------
        sftp: sftp object
            client discussing with sftp server.

        Returns
        -------
        None
        """

        if os.path.exists(os.getcwd() + "/" + self.tgz_name):
            os.remove(os.getcwd() + "/" + self.tgz_name)
        if self.zipfile is not None:
            for file in self.zipfile.infolist():
                if os.path.exists(os.getcwd() + "/" + file.filename):
                    os.remove(os.getcwd() + "/" + file.filename)

        if sftp is not None:
            sftp.close()
