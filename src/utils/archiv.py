
import logging
import tarfile
import os
from zipfile import ZipFile

logging.basicConfig(filename="./tmp/myapp.log",
                    format='%(asctime)s %(levelname)s %(message)s',
                    level=logging.INFO,
                    datefmt='%Y-%m-%d %H:%M:%S',
                    )


def get_all_file(directory):
    # initializing empty file paths list
    file_paths = []
    # crawling through directory and subdirectories
    for root, directories, files in os.walk(directory+'.'):
        for filename in files:
            # join the two strings in order to form the full filepath.
            filepath = os.path.join(root, filename)
            file_paths.append(filepath)
    # returning all file paths
    return file_paths


def unzip_file(file):
    try:
        with ZipFile(file, 'r') as zip:
            zip.printdir()
            print('Extracting all the files now...')
            zip.extractall()
            print('Done!')
    except Exception as e:
        logging.error(e)
        print(e)


def zip_file(file, directory):
    try:
        file_paths = get_all_file(directory)
        if file_paths:
            with ZipFile(file+'.zip', 'w') as zip:
                print('Packing all the files now...')
                for files in file_paths:
                    zip.write(files)
                print('Done!')
        else:
            raise Exception("The folder is empty")
    except Exception as e:
        logging.error(e)
        print(e)


def tar_file(file, directory):
    try:
        file_paths = get_all_file(directory)
        if file_paths:
            with tarfile.open(file+'.tar.gz', "w:gz") as tar:
                print('Packing all the files now...')
                for files in file_paths:
                    tar.add(files)
                print('Done!')
        else:
            raise Exception("The folder is empty")
    except Exception as e:
        logging.error(e)
        print(e)


def untar_file(file):
    try:
        with tarfile.open(file+'.tar.gz', 'r:gz') as tar:
            print('Extracting all the files now...')
            tar.extractall()
            print('Done!')
    except Exception as e:
        logging.error(e)
        print(e)
