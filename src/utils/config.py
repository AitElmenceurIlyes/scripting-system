import configparser
import logging

logging.basicConfig(filename="./tmp/myapp.log",
                    format='%(asctime)s %(levelname)s %(message)s',
                    level=logging.INFO,
                    datefmt='%Y-%m-%d %H:%M:%S',
                    )

# generate_default_config()


def get_info(config, sectionname, key):
    """
    getInfo

    extract information from ini file

    Parameters
    ----------
    config : string
        ini file location
    sectionname : string
        name of a section
    key :
        name of a key

    Returns
    -------
    string
        value
    """
    config_object = configparser.ConfigParser(interpolation=None)
    config_object.read(config)
    distantserv = config_object[sectionname]
    return str(distantserv[key])


def update_info(sectionname, key, new_value):
    """
    getInfo

    update information from ini file

    Parameters
    ----------
    config : string
        ini file location
    sectionname : string
        name of a section
    key :
        name of a key
    new_value : string
        new value

    Returns
    -------
    None
    """
    try:
        config_object = configparser.ConfigParser()
        config_object.read("./config.ini")
        section = config_object[sectionname]
        if section[key]:
            section[key] = str(new_value)
            with open('./config.ini', 'w') as conf:
                config_object.write(conf)
    except KeyError:
        logging.error("this key does'nt exist")
    except Exception as e:
        logging.error(e)


def generate_default_config():
    """
    generateDefaultConfig

    Generate defult config file
    """
    config = configparser.RawConfigParser()
    config.add_section('http')
    config.set('http', 'host', 'localhost')
    config.set('http', 'ipaddr', '')
    config.set('http', 'port', '8080')
    config.set('http', 'zip', 'zipname.zip')
    config.set('http', 'file', 'dumpfile')
    config.add_section('smb')
    config.set('smb', 'ip', '')
    config.set('smb', 'sharespace', '')
    config.set('smb', 'username', '')
    config.set('smb', 'machine_name', '')
    config.set('smb', 'pwd', '')
    config.set('smb', 'timetosave', '1')
    config.add_section('nfs')
    config.set('nfs', 'ip', '')
    config.set('nfs', 'sharespace', '')
    config.set('nfs', 'machine_name', '')
    config.set('nfs', 'username', '')
    config.set('nfs', 'pwd', '')
    config.set('nfs', 'timetosave', '1')
    config.add_section('email')
    config.set('email', 'send-emails', 'no')
    config.set('email', 'email', "")
    config.set('email', 'pwd', "")
    config.set('email', 'smtp', "")
    config.set('email', 'port', "")
    config.set('email', 'log-file-attached', "yes")
    config.set('email', 'title', "")
    config.set('email', 'destination', "")
    config.add_section('mattermost')
    config.set('mattermost', 'url', '')
    config.set('mattermost', 'token', "")
    config.set('mattermost', 'notification', "")
    config.set('mattermost', 'channel', "")
    with open('config.ini', 'w') as configfile:
        config.write(configfile)


if __name__ == '__main__':
    generate_default_config()
