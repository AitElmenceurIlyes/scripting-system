# -*- coding: utf-8 -*-

import datetime
import os

from pyNfsClient import (DATA_SYNC, MNT3_OK, NFS3_OK, NFS_PROGRAM, NFS_V3,
                         Mount, NFSv3, Portmap)

import utils.config as cf


class NFSServer:

    """
    Manage operations to send files to nfs server.

    Attributes
    ----------
    notification_sender: NotificationSender
        Object to manage all logs.
    time_to_save: int
        Time in days to store data on server.
    ip: string
        IP of nfs server.
    user: string
        Username of user to connect.
    pwd: string
        Password to connect to user.
    is_date_ok: boolean
        Comparison of last modification date of dump file and today.

    Methods
    -------
    connect(action):
        Connect to nfs server with ssh keys.
    send_to_nfs_server(tgz_name_):
        Send tgz file to nfs server.
    archival_check():
        Check and remove old tgz files depending on the time to save files.
    check_file_ack(tgz_name_):
        Check if nfs server has tgz file.
    close():
        Close nfs connection if this one is still opened.

    """

    def __init__(self, config, logging, is_date_ok):
        """
        Constructor of the nfsServer class

        Parameters
        ----------
        nfs_server_infos: dict
            contains all server infos such as ip address, username ...
        logging: logging object
            object to manage logs
        is_date_ok: boolean
            True if the last modification date of the file is today, else False

        Returns
        -------
        None.

        """

        self.notification_sender = logging

        self.user = cf.get_info(config, "nfs", "username")
        self.pwd = cf.get_info(config, "nfs", "pwd")
        self.ip = cf.get_info(config, "nfs", "ip")
        self.sharespace = cf.get_info(config, "nfs", "sharespace")
        self.machine_name = cf.get_info(config, "nfs", "machine_name")
        self.auth = {"flavor": 1,
                     "machine_name": self.machine_name,
                     "uid": 0,
                     "gid": 0,
                     "aux_gid": list(),
                     }
        self.is_date_ok = is_date_ok

        try:
            time_to_save_ = int(cf.get_info(
                config, "nfs", "timetosave"))
        except ValueError:
            time_to_save_ = 10
            self.notification_sender.warning("Ini Error",
                                             "error while reading Time to save value format is not supported. Default value is 10 days.",)
        self.time_to_save = time_to_save_

        self.nfs = None
        self.nfs = self.connect("nfs connection")

    def connect(self, action):
        """
        Connect to nfs server by managing ssh keys.

        Parameters
        ----------
        action: string
            Current action written in logs (ACK or auth)

        Returns
        -------
        nfs server object.

        """

        mount_path = "/mnt/"+self.sharespace

        try:
            portmap = Portmap(self.ip, timeout=3600)
            portmap.connect()
            mnt_port = portmap.getport(Mount.program, Mount.program_version)
            mount = Mount(self.ip, mnt_port, 3600, self.auth)
            mount.connect()
            self.notification_sender.info(
                action,
                "Connection established with nfs server @" + self.ip,
            )
            nfs_port = portmap.getport(NFS_PROGRAM, NFS_V3)
            # nfs actions
            self.nfs = NFSv3(self.ip, nfs_port, 3600, self.auth)
            return True
        except Exception as e:
            self.notification_sender.error(
                action, "Connection error occured "+str(e))

    def send_to_nfs_server(self, tgz_name_):
        """
        Send tgz file to nfs server.

        Parameter
        ---------
        tgz_name_: string
            name of tar.gz file to send to server.

        Returns
        -------
        None.

        """

        try:
            if self.connect('Post'):
                # upload file to /data/guest/upload on remote
                with self.nfs.cd("data"):
                    self.nfs.put(tgz_name_)
                os.remove(tgz_name_)

                self.notification_sender.info("Send to nfs")

        except (IOError, OSError) as err:
            if err is OSError:
                path = "Local"
            else:
                path = "Remote"
                self.notification_sender.error(
                    "Send to nfs ", path + " path does not exists.")

        finally:
            if self.nfs is not None:
                self.close()

    def archival_check(self):
        """
        Check and remove old tgz files depending on the time to save files.

        Parameters
        ----------

        Returns
        -------
        None.

        """

        try:
            if self.nfs is not None:
                # Current directory.
                files = self.nfs.listdir("data")
                dead_line = (
                    datetime.datetime.today()
                    - datetime.timedelta(days=self.time_to_save)
                ).date()
                for file in files:
                    try:
                        date = datetime.datetime.strptime(
                            file.replace(".tgz", ""), "%Y%d%m"
                        ).date()
                        if date < dead_line:
                            with self.nfs.cd("data"):
                                self.nfs.remove(file)
                        self.notification_sender.info("nfs archival")

                    except ValueError:
                        pass

            else:
                self.notification_sender.error(
                    "nfs archival", "Connection to nfs is not done."
                )
                self.notification_sender.error(
                    "Send to nfs", "Sending to nfs server not done."
                )
                self.notification_sender.error("ACK", "Checking ACK not done.")

        except IOError:
            self.notification_sender.error(
                "nfs archival", "Remote path does not exist.")

        finally:
            if not self.is_date_ok and self.nfs is not None:
                self.notification_sender.warning(
                    "Send to nfs",
                    "Sending to nfs server not done because dates do not correspond.",
                )
                self.notification_sender.warning(
                    "ACK", "Checking ACK not done because dates do not correspond."
                )
                self.close()

    def check_file_ack(self, tgz_name_):
        """
        Check if nfs server has tgz file.

        Parameters
        ----------
        tgz_name_: string
            tar.gz filename to check ack on nfs server.

        Returns
        -------
        None.

        """

        nfs = None
        try:
            nfs = self.connect("ACK")
            with nfs.cd("data"):
                files = nfs.listdir()
                for file in files:
                    if file == tgz_name_:
                        self.notification_sender.info(
                            "ACK", "Tar file sent to nfs server.")
                        return
            nfs.close()

        except pynfs.ConnectionException:
            self.notification_sender.error("ACK", "Connection error occured.")

        except pynfs.AuthenticationException:
            self.notification_sender.error(
                "ACK", "Authentication error occured.")

        except pynfs.HostKeysException:
            self.notification_sender.error(
                "ACK", "Loading host keys error occured.")

        except pynfs.SSHException:
            self.notification_sender.error("ACK", "Unknow SSH error occured.")

        except IOError:
            self.notification_sender.error(
                "ACK", "Remote path does not exists.")

        finally:
            if nfs is not None:
                nfs.close()

    def close(self):
        """
        Close nfs connection if this one is still opened.

        Parameters
        ----------

        Returns
        -------
        None.

        """

        if self.nfs is not None:
            self.nfs.disconnect()
