import archiver as ar
import smb_handler as smbh

arc = ar.Archiver(8000, "./config.ini")
arc.get_config()
arc.get_zip()
arc.extract_zip()
if arc.has_file():
    arc.compress_to_tgz()
    # File date is not the same than before - file has been changed today
    is_date_ok = arc.compare_date()

    smb = smbh.SMBServer("./config.ini", arc.notification_sender, is_date_ok)
    smb.archival_check()

    if is_date_ok:
        smb.send_to_smb_server(arc.tgz_name)
        smb.check_file_ack(arc.tgz_name)

    arc.notification_sender.send_all()
    arc.clean(smb)
