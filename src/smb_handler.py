# -*- coding: utf-8 -*-

import datetime
import os

from smb.SMBConnection import SMBConnection

import utils.config as cf


class SMBServer:

    """
    Manage operations to send files to smb server.

    Attributes
    ----------
    notification_sender: NotificationSender
        Object to manage all logs.
    time_to_save: int
        Time in days to store data on server.
    ip: string
        IP of smb server.
    user: string
        Username of user to connect.
    pwd: string
        Password to connect to user.
    is_date_ok: boolean
        Comparison of last modification date of dump file and today.

    Methods
    -------
    connect(action):
        Connect to smb server with ssh keys.
    send_to_smb_server(tgz_name_):
        Send tgz file to smb server.
    archival_check():
        Check and remove old tgz files depending on the time to save files.
    check_file_ack(tgz_name_):
        Check if smb server has tgz file.
    close():
        Close smb connection if this one is still opened.

    """

    def __init__(self, config, logging, is_date_ok):
        """
        Constructor of the smbServer class

        Parameters
        ----------
        smb_server_infos: dict
            contains all server infos such as ip address, username ...
        logging: logging object
            object to manage logs
        is_date_ok: boolean
            True if the last modification date of the file is today, else False

        Returns
        -------
        None.

        """

        self.notification_sender = logging

        self.user = cf.get_info(config, "smb", "username")
        self.pwd = cf.get_info(config, "smb", "pwd")
        self.ip = cf.get_info(config, "smb", "ip")
        self.share_space = cf.get_info(config, "smb", "sharespace")
        self.machin_name = cf.get_info(config, "smb", "machine_name")

        self.is_date_ok = is_date_ok

        try:
            time_to_save_ = int(cf.get_info(
                config, "smb", "timetosave"))
        except ValueError:
            time_to_save_ = 10
            self.notification_sender.warning("Ini Error",
                                             "error while reading Time to save value format is not supported. Default value is 10 days.",)
        self.time_to_save = time_to_save_

        self.smb = None

    def connect(self, action):
        """
        Connect to smb server by managing ssh keys.

        Parameters
        ----------
        action: string
            Current action written in logs (ACK or auth)

        Returns
        -------
        smb server object.

        """

        try:
            self.smb = SMBConnection(
                self.user, self.pwd, self.machin_name, self.share_space, use_ntlm_v2=True)
            self.smb.connect(self.ip, 139)
            self.notification_sender.info(
                action,
                "Connection established with smb server @" + self.ip,
            )
            return True

        except Exception as e:
            self.notification_sender.error(
                action, "Connection error occured : "+str(e))
            return False

    def send_to_smb_server(self, tgz_name_):
        """
        Send tgz file to smb server.

        Parameter
        ---------
        tgz_name_: string
            name of tar.gz file to send to server.

        Returns
        -------
        None.

        """

        try:
            if self.connect("Connection"):
                # upload file to /data/guest/upload on remote
                file = open('./'+tgz_name_, 'rb')
                self.smb.storeFile(self.share_space, '/data/'+tgz_name_, file)
                file.close()
                os.remove(tgz_name_)

                self.notification_sender.info("Send to smb")

        except (IOError, OSError) as err:
            if err is OSError:
                path = "Local"
            else:
                path = "Remote"
                self.notification_sender.error(
                    "Send to smb", path + " path does not exists.")

        finally:
            if self.smb is not None:
                self.close()

    def archival_check(self):
        """
        Check and remove old tgz files depending on the time to save files.

        Parameters
        ----------

        Returns
        -------
        None.

        """

        try:
            if self.connect("Connection"):
                # Current directory.
                files = self.smb.listPath(self.share_space, '/data')
                dead_line = (
                    datetime.datetime.today()
                    - datetime.timedelta(days=self.time_to_save)
                ).date()
                for file in files:
                    if(file.filename == '.' or file.filename == '..'):
                        pass
                    else:
                        try:
                            date = datetime.datetime.strptime(
                                file.filename.replace(".tgz", ""), "%Y%d%m"
                            ).date()
                            if date < dead_line:
                                self.smb.deleteFiles(
                                    self.share_space, '/data/'+file.filename, delete_matching_folders=False, timeout=30)
                            self.notification_sender.info("Archive checkup")

                        except ValueError:
                            pass
            else:
                self.notification_sender.error(
                    "Archive checkup", "Connection to smb is not done."
                )
                self.notification_sender.error(
                    "Archive checkup", "Sending to smb server not done."
                )
                self.notification_sender.error("Archive checkup", "Checking ACK not done.")

        except IOError:
            self.notification_sender.error(
                "Archive checkup", "Remote path does not exist.")

        finally:
            if not self.is_date_ok and self.smb is not None:
                self.notification_sender.warning(
                    "Archive checkup",
                    "Sending to smb server not done because dates do not correspond.",
                )
                self.notification_sender.warning(
                    "Archive checkup", "Checking ACK not done because dates do not correspond."
                )
            self.close()

    def check_file_ack(self, tgz_name_):
        """
        Check if smb server has tgz file.

        Parameters
        ----------
        tgz_name_: string
            tar.gz filename to check ack on smb server.

        Returns
        -------
        None.

        """

        try:
            if self.connect("Connection"):
                files = self.smb.listPath(self.share_space, '/data')
                for file in files:
                    if file.filename == tgz_name_:
                        self.notification_sender.info(
                            "Tar in the server", "Tar file sent to smb server.")
                        return

        except IOError:
            self.notification_sender.error(
                "Tar in the server", "Remote path does not exists.")
        except Exception as e:
            self.notification_sender.error(
                "Tar in the server", "Connection error occured: " + e)

        finally:
            if self.smb is not None:
                self.close()

    def close(self):
        """
        Close smb connection if this one is still opened.

        Parameters
        ----------

        Returns
        -------
        None.

        """

        if self.smb is not None:
            self.smb.close()
