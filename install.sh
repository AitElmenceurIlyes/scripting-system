#!/bin/bash

python3 --version

sudo apt-get install --upgrade python3

sudo apt-get install python3-pip

python3 -m pip --version


python3 -m pip install configparser
python3 -m pip install email_validator
python3 -m pip install pysmb
python3 -m pip install urllib3
python3 -m pip install smbprotocol
python3 -m pip install psutil

# Configure rights on files
# Find all python, bash files and folders in this folder 
# and allow them just to be executed.
sudo find . -type f -iname "*.sh" -exec chmod 511 {} \;
sudo find . -type f -iname "*.py" -exec chmod 511 {} \;
sudo find . -type d ! -name "." -exec chmod 511 {} \;
# Config file can be change by anyone.
sudo find . -type f -iname "config.ini" -exec chmod 777 {} \;
# Log file can be read only.
sudo find . -type f -iname "myapp.log" -exec chmod 744 {} \;

# Create crontab and make archival automatic every day.
# Configure crontab to call main.py everyday at midnight.
# We suppose web server is launched 
# and we do not need to launch it manually.
# Each day at 11 pm (0 23) go to PWD (Scripting-System folder)
# and execute main.py. 
{ crontab -l -u $USER; echo "0 23 * * * cd $PWD; python3 main.py"; } | crontab -u $USER -

# Check crontab was created.
if sudo test -f "/var/spool/cron/crontabs/$USER"
then
	echo "Successfully crontab created in /var/spool/cron/crontabs."
	echo "Logs for this crontab are in /var/log/syslog."
	echo "To restart service, type sudo service cron restart/start."

	# Launch cron service
	sudo service cron start
else
	echo "Error: Cron file not created. Please create it manually as described in users' doc annexes."
fi
sudo python3 ./src/utils/config.py
